import sublime
import sublime_plugin
import urllib2
import httplib
from urlparse import urlparse
import re
from OpenSSL import SSL

class FetchAddCommand(sublime_plugin.WindowCommand):
    def run(self):
        sublime.set_timeout(FetchUpdater.instance().run_add, 10)

class FetchRemoveCommand(sublime_plugin.WindowCommand):
    def run(self):
        sublime.set_timeout(FetchUpdater.instance().run_remove, 10)

class FetchUpdateCommand(sublime_plugin.WindowCommand):
    def run(self):
        sublime.set_timeout(FetchUpdater.instance().run_update, 10)



class FetchUpdater(object):

    strategies = None
    fetcherSettings = None

    types = None
    types_remove = None
    

    _instance = None

    @staticmethod
    def instance():
        if not FetchUpdater._instance:
            FetchUpdater._instance = FetchUpdater()
        return FetchUpdater._instance

    def run_update(self):
        
        updaterSettings = sublime.load_settings('FetchUpdater.sublime-settings')
        self.strategies = updaterSettings.get('strategies', {})
        fetcherSettings = sublime.load_settings('Fetch.sublime-settings');
        files = fetcherSettings.get('files', []);

        updated = False
        for file in files:
            updated_url = self.update(file)
            
            if updated_url and files[file] != updated_url:
                files[file] = updated_url
                updated = True

        if updated:
            fetcherSettings.set('files', files)
            sublime.save_settings('Fetch.sublime-settings')



    def run_add(self):
        updaterSettings = sublime.load_settings('FetchUpdater.sublime-settings')
        self.strategies = updaterSettings.get('strategies', {})

        fetcherSettings = sublime.load_settings('Fetch.sublime-settings');
        files = fetcherSettings.get('files', []);

        self.types = []

        for type in self.strategies:

            if not type in files:
                self.types.append(type)

        self.types = sorted(self.types)

        if len(self.types) > 0:
            sublime.active_window().show_quick_panel(self.types, self.on_add_select)



    def on_add_select(self, selected):
        print selected
        try:
            type = self.types[selected]
            print type

            fetcherSettings = sublime.load_settings('Fetch.sublime-settings');
            files = fetcherSettings.get('files', []);

            files[type] = self.update(type)

            fetcherSettings.set("files", files)
            sublime.save_settings('Fetch.sublime-settings')

        except Exception, c:
            print 

    def run_remove(self):
        print 'fetch_remove'

        fetcherSettings = sublime.load_settings('Fetch.sublime-settings');
        files = fetcherSettings.get('files', []);

        self.types_remove = []

        for type in files:
            self.types_remove.append(type)

        if len(self.types_remove) > 0:
            sublime.active_window().show_quick_panel(self.types_remove, self.on_remove_select)
    
    def on_remove_select(self, selected):
        try:
            type = self.types_remove[selected]
            print type

            fetcherSettings = sublime.load_settings('Fetch.sublime-settings');
            files = fetcherSettings.get('files', []);

            del files[type]

            fetcherSettings.set("files", files)
            sublime.save_settings('Fetch.sublime-settings')

        except Exception, c:
            print 

    def update(self, type):

        print "update " + type 
        if not type in self.strategies:
            print "No update strategy for " + type
            return False
        
        strategy = self.strategies[type]

        if "file_url" in strategy:
            return strategy["file_url"]

        respHtml = self.get(strategy["url"])
        ret = re.search(strategy['pattern'], respHtml)

        url = ret.group(1)

        if re.search('^//', url):
            url = "http:" + url 

        print "file_url = " + url
        return url

    def get(self, url):

        respHtml = "";
        o = urlparse(url)

        print "GET " + url

        if re.search('^https', url):
            port = 443
            httpServ = httplib.HTTPSConnection(o.netloc)
        else:
            httpServ = httplib.HTTPConnection(o.netloc)

        httpServ.connect()
        httpServ.request('GET', o.path)

        response = httpServ.getresponse()
        if response.status == httplib.OK:
            respHtml = response.read()

        httpServ.close()

        return respHtml
